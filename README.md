|         | sol单价 | 到手SOL     | 到手U                   |
|---------|-------|-----------|-----------------------|
| 买入200U  | 100U  | 2-2*0.0001 |                       | 
| 卖出到手SOL | 101U  |           | 到手SOL*101U(1-0.00008) |

盈利: 到手SOL*101U(1-0.00008) > 200U

代码实现:

买入U数量 buySize 

买入币单价(U) buyPrize

卖出币单价(U) sellPrize

买入手续费  buyTicket

卖出手续费  sellTicket

到手币数量 = buySize/buyPrize(1-buyTicket)

到手U = 到手币数量 * sellPrize (1-sellTicket)

是否盈利 = 到手U-buySize > 0