import time
import asyncio
from utils.emilUtil import sendEmails
from utils.notification import show_notification
from okx import PublicData, Trade


# 异步发送消息
async def sendMessage(text):
    sendEmails(text)
    show_notification('币', text)


class SOL:
    def __init__(self, instId):
        api_key = ''
        api_secret_key = ''
        passphrase = ''
        self.publicDataApi = PublicData.PublicAPI(api_key, api_secret_key, passphrase, use_server_time=False, flag='0')
        self.tradeApi = Trade.TradeAPI(api_key, api_secret_key, passphrase, False, '0')
        self.instId = instId

    # 获取标记价格
    # 为了防止个别用户恶意操控市场导致合约价格波动剧烈，我们根据现货指数和合理基差设定标记价格。
    def get_mark_price(self):
        result = self.publicDataApi.get_mark_price('SWAP', '', self.instId + '-USD-SWAP')
        amount = result['data'][0]['markPx']
        return amount

    # 获取指数行情数据
    def get_index_tickers(self):
        result = self.publicDataApi.get_index_tickers('USD', self.instId + '-USD')
        amount = result['data'][0]['idxPx']
        return amount

    # 买入
    def place_order_buy(self, ordType):
        result = self.tradeApi.place_order(self.instId + "-USDT", tdMode="cash", clOrdId=self.instId + 'wxy0715',
                                           side="buy", ordType=ordType, sz=buySize)
        return result['data'][0]['ordId']

    # 卖出
    def place_order_sell(self, ordType):
        result = self.tradeApi.place_order(self.instId + "-USDT", tdMode="cash", clOrdId=self.instId + 'wxy0715',
                                           side="sell", ordType=ordType, sz=buySize)
        return result['data'][0]['ordId']


# 买入手续费
buyTicket = 0.0001
# 卖出手续费
sellTicket = 0.00008
# 买入币价格
buyPrize = '62'
# 卖出是否重试
sellRetry = True

if __name__ == '__main__':
    ccy = 'SOL'
    # 买入U数量
    buySize = '260'
    # 构造api
    buy = SOL(ccy)
    while True:
        try:
            if sellRetry:
                # 到手币数量
                realCcySize = float(buySize) / float(buyPrize) * (1 - buyTicket)
            else:
                # 获取现在币价格
                buyPrize = buy.get_index_tickers()
                # 到手币数量
                realCcySize = float(buySize) / float(buyPrize) * (1 - buyTicket)
                # 下单
                try:
                    place_order_buy = buy.place_order_buy('market')
                    print("买入" + ccy + "单价:[" + buyPrize + "(U)]数量:[" + str(realCcySize) + "]")
                    print(ccy + '买入订单id:' + place_order_buy)
                except Exception as e:
                    print(e)
                    asyncio.run(sendMessage("买入程序发生异常->买入币价格为:[" + buyPrize + "]数量:" + buySize))
                finally:
                    sellRetry = False
            # 实时请求
            while True:
                time.sleep(2)
                # 获取现在币价格
                sellPrize = buy.get_index_tickers()
                profit = realCcySize * float(sellPrize) * (1 - sellTicket) - float(buySize)
                print("目前币单价为(U):" + sellPrize, "目前盈利数量为:" + str(profit))
                # 是否盈利
                if profit > 0:
                    # 卖出
                    place_order_sell = buy.place_order_sell('market')
                    sellRetry = False
                    print(ccy + '卖出订单id:' + place_order_sell)
                    # 发送消息
                    asyncio.run(sendMessage(ccy + "盈利数量" + str(profit)))
                    break
        except Exception as e:
            print(e)
            asyncio.run(sendMessage("卖出程序发生异常->买入币价格为:[" + buyPrize + "]数量:" + buySize))
            sellRetry = True
