import unittest

from utils.emilUtil import sendEmails
from utils.notification import show_notification
from okx import PublicData


class publicDataTest(unittest.TestCase):
    def setUp(self):
        api_key = ''
        api_secret_key = ''
        passphrase = ''
        self.publicDataApi = PublicData.PublicAPI(api_key, api_secret_key, passphrase, use_server_time=False, flag='0')

    '''
    TestCase For:
    INTEREST_LOAN = '/api/v5/public/interest-rate-loan-quota' #need to add
    UNDERLYING = '/api/v5/public/underlying' #need to add
    VIP_INTEREST_RATE_LOAN_QUOTA = '/api/v5/public/vip-interest-rate-loan-quota' #need to add
    INSURANCE_FUND = '/api/v5/public/insurance-fund'#need to add
    CONVERT_CONTRACT_COIN = '/api/v5/public/convert-contract-coin' #need to add
    def test_interest_loan(self):
        print(self.publicDataApi.get_interest_rate_loan_quota())
    def test_get_underlying(self):
        print(self.publicDataApi.get_underlying("SWAP"))
    def test_get_vip_loan(self):
        print(self.publicDataApi.get_vip_interest_rate_loan_quota())
    def test_insurance_fund(self):
        print(self.publicDataApi.get_insurance_fund("SWAP",uly= "BTC-USD"))
    def test_convert_contract_coin(self):
        print(self.publicDataApi.get_convert_contract_coin(instId="BTC-USD-SWAP",sz = "1",px = "27000"))
    def test_get_instruments(self):
        print(self.publicDataApi.get_instruments("SPOT"))
    def test_delivery_exercise_history(self):
        print(self.publicDataApi.get_deliver_history("FUTURES","BTC-USD"))
    def test_get_open_interest(self):
        print(self.publicDataApi.get_open_interest("SWAP"))
    def test_get_funding_rate(self):
        print(self.publicDataApi.get_funding_rate("BTC-USD-SWAP"))
    def test_get_funding_rate_history(self):
        print(self.publicDataApi.funding_rate_history('BTC-USD-SWAP'))
    def test_get_opt_summary(self):
        print(self.publicDataApi.get_opt_summary('BTC-USD'))

    def test_estimate_price(self):
        print(self.publicDataApi.get_estimated_price("BTC-USD-220831-17000-P"))
    def test_get_discount_rate_interest(self):
        print(self.publicDataApi.discount_interest_free_quota(ccy='ETH'))
    def test_get_systime(self):
        print(self.publicDataApi.get_system_time())
    def test_get_liquid_order(self):
        print(self.publicDataApi.get_liquidation_orders("SWAP",uly='BTC-USD',state='filled'))
    
    '''

    # def test_position_tier(self):
    #     print(self.publicDataApi.get_position_tiers('SWAP','cross',uly='ETH-USD'))

    # def test_get_option_tickBands(self):
    #     print(self.publicDataApi.get_option_tick_bands(instType='OPTION'))

    # GET / 获取期权公共成交数据
    # 最多返回最近的100条成交数据
    def test_get_option_trades(self):
        print(self.publicDataApi.get_option_trades(instFamily='SOL-USDT'))

    # 获取交易产品基础信息
    # 获取所有可交易产品的信息列表。
    def test_get_instruments(self):
        print(self.publicDataApi.get_instruments("SPOT", '', 'SOL-USDT')['data'])

    # 获取限价
    # 查询单个交易产品的最高买价和最低卖价
    def test_get_price_limited(self):
        print(self.publicDataApi.get_price_limit("SOL-USD-SWAP"))

    # 获取标记价格
    # 为了防止个别用户恶意操控市场导致合约价格波动剧烈，我们根据现货指数和合理基差设定标记价格。
    def test_get_mark_price(self):
        result = self.publicDataApi.get_mark_price('SWAP', '', 'SOL-USD-SWAP')
        print("获取标记价格")
        print(result)
        sendEmails("SOL的标记价格为" + result['data'][0]['markPx'])

    # 获取指数行情数据
    def test_get_index_tickers(self):
        result = self.publicDataApi.get_index_tickers('USD', 'SOL-USD')
        print("获取指数行情数据")
        print(result)
        #sendEmails("SOL的指数行情价格为" + result['data'][0]['idxPx'])
        show_notification('SOL', "SOL的指数行情价格为" + result['data'][0]['idxPx'])


if __name__ == '__main__':
    unittest.main()
