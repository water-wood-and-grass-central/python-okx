import os
import subprocess


# macos通知
def show_notification(title, text):
    os.system("""
              osascript -e 'display notification "{}" with title "{}"'
              """.format(text, title))


# macos通知
def show_notification_2(title, text):
    cmd = 'display notification \"' + \
          text + '\" with title \"' + title + '\"'
    subprocess.call(["osascript", "-e", cmd])
