import smtplib
from email.mime.text import MIMEText

# 设置服务器所需信息

# 邮箱服务器地址
mail_host = 'smtp.qq.com'
# 邮箱登录用户名
mail_user = ''
# 密码(部分邮箱为授权码)
mail_pass = ''
# 邮件发送方邮箱地址
sender = ''
# 邮件接受方邮箱地址，注意需要[]包裹，这意味着你可以写多个邮件地址群发
receivers = []


def sendEmails(text):
    print('邮箱内容:'+text)
    # 邮件内容设置
    message = MIMEText('', 'plain', 'utf-8')
    # 邮件主题
    message['Subject'] = text
    # 发送方信息
    message['From'] = sender
    # 接受方信息
    message['To'] = receivers[0]
    try:
        smtpObj = smtplib.SMTP()
        smtpObj.connect(mail_host, 25)
        # 登录到服务器
        smtpObj.login(mail_user, mail_pass)
        # 发送
        smtpObj.sendmail(sender, receivers, message.as_string())
        # 退出
        smtpObj.quit()
        print('邮箱发送成功')
    except smtplib.SMTPException as e:
        print('error', e)

